package co.com.matrixtech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.matrixtech.business.domain.Tercero;
import co.com.matrixtech.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public List<Tercero> findAllCustomers() throws Exception {
		try {
			return customerRepository.findAllCustomers();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
