package co.com.matrixtech.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableAutoConfiguration
@SpringBootApplication
@EnableWebMvc
@ComponentScan(basePackages = "co.com.matrixtech.*")
public class Application {

	protected Application() {}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
