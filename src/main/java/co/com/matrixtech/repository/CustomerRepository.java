package co.com.matrixtech.repository;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.com.matrixtech.business.domain.Tercero;
import co.com.matrixtech.business.repository.TerceroRepository;

@Repository
public class CustomerRepository {

	@Autowired
	private TerceroRepository terceroRepository;

	public List<Tercero> findAllCustomers() throws SQLException {
		return terceroRepository.consultarTodos();
	}
}
