package co.com.matrixtech.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import co.com.matrixtech.business.domain.Tercero;
import co.com.matrixtech.service.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/terceros/all", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Tercero> findAllCustomers() {
		try {
			return customerService.findAllCustomers();
		} catch (Exception e) {
			System.out.println("Error: " + e);
			return new ArrayList<>();
		}
	}
}
